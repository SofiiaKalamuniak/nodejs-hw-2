const User = require('./user');
const Notes = require('./notes');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const { JWTSecret } = require('./config/index');

const generateAccessToken = (id) => {
  const payload = {
    id
  };
  return jwt.sign(payload, JWTSecret, { expiresIn: '24h' });
};

class AuthController {
  async registration(req, res) {
    try {
      const err = validationResult(req);
      if (!err.isEmpty()) {
        return res.status(400).send({ message: 'Error message' });
      }
      const { username, password } = req.body;
      const candidate = await User.findOne({ username });
      if (candidate) {
        return res.status(400).send({ message: 'Error message' });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const user = new User({
        username,
        password: hashPassword,
        createdDate: new Date()
      });
      await user.save();
      return res.status(200).send({ message: 'Success' });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: 'Error message' });
    }
  }

  async login(req, res) {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({ username });
      if (!user) {
        return res.status(400).send({ message: 'Error message' });
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).send({ message: 'Error message' });
      }
      const token = generateAccessToken(user._id);
      return res.status(200).send({
        message: 'Success',
        jwt_token: token
      });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: 'Error message' });
    }
  }

  async getUser(req, res) {
    try {
      console.log(req.user.id);
      const user = await User.findById(req.user.id);
      const result = {
        user: {
          _id: user.id,
          username: user.username,
          createdDate: user.createdDate
        }
      };
      res.status(200).send(result);
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async createNotes(req, res) {
    try {
      const notes = new Notes({
        userId: req.user.id,
        completed: false,
        text: req.body.text,
        createdDate: new Date()
      });
      await notes.save();
      res.status(200).send({ message: 'Success' });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async getNotes(req, res) {
    try {
     await User.findOne({ id: req.user.id });

      const paramsString = req.url.split('?')[1];
      const eachParamArray = paramsString.split('&');
      let params = {};
      eachParamArray.forEach((param) => {
        params[param.split('=')[0]] = param.split('=')[1];
      });

      const all = await Notes.find({ userId: req.user.id });
      const notes = await Notes.find({ userId: req.user.id })
        .skip(parseInt(params.offset))
        .limit(parseInt(params.limit))
        .select('-__v');

      return res.status(200).send({
        offset: params.offset,
        limit: params.limit,
        count: all.length,
        notes: notes
      });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async checkNote(req, res) {
    try {
      const pre = await Notes.findOne({ _id: req.params.id });
      await Notes.findOneAndUpdate(
        { _id: req.params.id },
        { completed: !pre.completed }
      );
      res.status(200).send({
        message: 'Success'
      });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async getNote(req, res) {
    try {
      const note = await Notes.findById(req.params.id).select('-__v');
      return res.status(200).send({
        note: note
      });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async updateNote(req, res) {
    try {
       await Notes.findOneAndUpdate(
        { _id: req.params.id },
        { text: req.body.text }
      );
      res.status(200).send({
        message: 'Success'
      });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async deleteNote(req, res) {
    try {
      Notes.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
          res.status(400).send({ message: 'Error message' });
        }
        res.status(200).send({
          message: 'Success'
        });
      });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }
}

module.exports = new AuthController();
