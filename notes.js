const {Shema, model, Schema} = require('mongoose')

const Notes = new Schema({
    userId: {type: String, required: true},
    completed: {type: Boolean, required: true},
    text: {type: String, required: true},
    createdDate: {type: Date, required: true}
})


module.exports = model('Notes', Notes)